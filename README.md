# Documents



Liste des fichiers representant les travaux effectues en premiere et deuxieme annees.

## Stages 

Rapport de stage 1 et 2 

- [ ] [Stage premiere annee](https://gitlab.com/mcci-bs/pierre_carrel/documents/-/blob/main/BTS_SIO22-24_ATTESTATION%20DE%20STAGE_1ereAnnee_02344518689_FOTSO%20YOUBISSI%20PIERRE%20CARREL.pdf?ref_type=heads)
- [ ] [Stage deuxieme annee](https://gitlab.com/mcci-bs/pierre_carrel/documents/-/blob/main/BTS_SIO22-24_ATTESTATION%20DE%20STAGE_2emeAnnee_02344518689_FOTSO%20YOUBISSI%20PIERRE%20CARREL.pdf?ref_type=heads)
***

## Modelisation

UML et Schema de donnees

- [ ] [Diagramme de classe UML](#)
- [ ] [Schema de donnees](#)
***